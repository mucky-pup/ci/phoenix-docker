FROM elixir:1.15.7-otp-26-slim

ENV DEBIAN_FRONTEND noninteractive
ENV TZ Etc/UTC

RUN apt-get update \
    && apt-get -y install tzdata \
    && apt-get -qq install -y git build-essential libssl-dev wget software-properties-common apt-transport-https curl

RUN curl -fsSL https://deb.nodesource.com/setup_20.x | bash -

RUN apt-get -y install nodejs

# Cypress dependencies
RUN apt-get -qq install -y \
    libgtk2.0-0 libgtk-3-0 libgbm-dev libnotify-dev libgconf-2-4 libnss3 libxss1 libasound2 libxtst6 xauth xvfb

# Elixir dependencies
RUN mix local.hex --force \
    && mix local.rebar --force \
    && apt-get -qq install -y inotify-tools
